import java.util.Scanner;

public class GcdV1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int a, b, residue, mcd = 0;

		Scanner entry = new Scanner(System.in);

		System.out.println("Enter the number 1");
		a = entry.nextInt();
		System.out.println("Enter the number 2");
		b = entry.nextInt();
		

		while (a % b != 0) {

			residue = a % b;
			a = b;
			b = residue;
			mcd = b;

		}

		System.out.println("The greatest common divisor is = " + mcd);
		entry.close();
	}
}
