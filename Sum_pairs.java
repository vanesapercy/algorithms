package practice_algorithms;

/*The sum of all even numbers between 2 and 1,000*/
public class Sum_pairs {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int num, sum;
		num = 2;
		sum = 0;

		while (num <= 1000) {

			sum = sum + num;
			num = num + 2;
		}

		System.out.println("The result is = " + sum);

	}

}
