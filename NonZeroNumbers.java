import java.util.Scanner;

public class NonZeroNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner entry = new Scanner(System.in);

		int num, countn = 0;

		do {
			System.out.println("Enter the number");
			num = entry.nextInt();

			if (num == 0) {

				System.out.println("Entered a zero, the execution ends");

			} else {
				System.out.println("Number entered " + num);
				countn += 1;
			}

		} while (num != 0);

		System.out.println("The quantity of numbers differents of zero is = " + countn);
		entry.close();

	}

}
