package practice_algorithms;

import java.util.Scanner;

public class Monthly_salary {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String name, answer = null;
		double price_hour = 0, salary;
		int hours = 0;
		
		Scanner entry = new Scanner(System.in);
		
		
		
		do {
			System.out.println("Enter name of employee");
			name = entry.next();
			
			System.out.println("Enter working hours");
			hours = entry.nextInt();
			
			System.out.println("Enter hour price");
			price_hour = entry.nextDouble();
			
			if(hours <= 40) {
				salary = hours * price_hour;
			}else {
				salary = (40 * price_hour)  + (1.5 * (hours - 40) * price_hour);
			}
			
			System.out.println("The salary of employee " + name + " is = " + salary);
			
			System.out.println("Do you want to enter more data S ?");
			answer = entry.next();
			
		}while(answer.equals("N"));

		entry.close();

	}
	
	
}

