package practice_algorithms;

import java.util.Scanner;

public class Count_Zerosv2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Count of zeros read by keyboard");
		int num, count_zeros = 0;
		String resp = "S";
		Scanner entry = new Scanner(System.in);

		while (resp.equalsIgnoreCase("s")) {

			System.out.println("Enter number");
			num = entry.nextInt();

			if (num == 0)
				count_zeros = count_zeros + 1;

			System.out.println("Would you like enter more numbers, type Y to continue or N to finish");
			resp = entry.next();

		}
		if (count_zeros > 0) {

			System.out.println("Total zeros is = " + count_zeros);

		} else {

			System.out.println("There are no zeros in the sequence");
		}
		entry.close();
	}
	
	
}