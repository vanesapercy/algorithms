package practice_algorithms;

import java.util.Scanner;

public class Count_Zeros {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int num, count_zero = 0;
		String response = null;
		Scanner entry = new Scanner(System.in);

		do {

			System.out.println("Enter number");
			num = entry.nextInt();

			if (num == 0) {
				count_zero = count_zero + 1;
			}
			System.out.println("Would you like enter more numbers, type Y to continue or N to finish");
			response = entry.next();
			
			//Mientras la respuesta sea igual a S, el ciclo se sigue repitiendo, de lo contrario termina
		} while (response.equalsIgnoreCase("y"));
		
		if(count_zero == 0) {
			System.out.println("There are no zeros in the sequence");
			
		}else {
			
			System.out.println("The sequence has " + count_zero + " zeros");
		}
		
		entry.close();

	}
}
