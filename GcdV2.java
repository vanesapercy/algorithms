/*
Write an algorithm to determine the greatest common divisor
 of two integers by the algorithm from Euclid.*/

import java.util.*;

public class GcdV2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int num1, num2, a, b, residue = 0;
		Scanner entry = new Scanner(System.in);

		System.out.println("Enter the number 1");
		num1 = entry.nextInt();

		System.out.println("Enter the number 2");
		num2 = entry.nextInt();

		//Determine the largest number and assign it
		if (num1 > num2) {
			a = num1;
			b = num2;
		} else {
			a = num2;
			b = num1;
		}
		
		//Loop for iterations of the residue
		while (b != 0) {
			residue = b;
			b = a % b;
			a = residue;
		}

		System.out.println("The greatest common divisor of " + num1 + " and " + num2 + " is = " + residue);
		entry.close();

	}

}
