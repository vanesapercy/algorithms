package practice_algorithms;

import java.util.Scanner;

public class Prime_numbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int num, residue, count_div = 0;

		Scanner entry = new Scanner(System.in);

		System.out.println("Enter number");

		num = entry.nextInt();

		for (int i = 1; i <= num; i++) {

			residue = num % i;

			if (residue == 0) {
				count_div = count_div + 1;
			}

		}

		if (count_div != 2) {
			System.out.println(num + " not is prime");
		} else {

			System.out.println(num + " is prime");
		}
	}

}
